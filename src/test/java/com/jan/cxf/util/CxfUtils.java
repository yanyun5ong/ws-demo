package com.jan.cxf.util;

import java.util.Map;

import javax.xml.ws.BindingProvider;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transport.Conduit;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;

public class CxfUtils {

    /**
     * 设置CXF客户端超时时间
     * 
     * @param port 服务接口
     * @param timeout 超时时间，单位毫秒
     */
    public static void recieveTimeOutWrapper(Object port, long timeout) {
        Client proxy = ClientProxy.getClient(port);
        HTTPConduit conduit = (HTTPConduit) proxy.getConduit();
        
        HTTPClientPolicy policy = new HTTPClientPolicy();
        policy.setReceiveTimeout(timeout);
        conduit.setClient(policy);
    }
    
}
