package com.jan.cxf.interceptor;

import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.databinding.DataBinding;
import org.apache.cxf.headers.Header;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.jaxb.JAXBDataBinding;
import org.apache.cxf.phase.Phase;

public class UnionOutInterceptor extends  AbstractSoapInterceptor{
	
	

	public UnionOutInterceptor() {
		super(Phase.WRITE);
	}

	@Override
	public void handleMessage(SoapMessage message) throws Fault {
		System.out.println("添加拦截器成功");
		
		List<Header> headers = message.getHeaders();  
        try {  
            //创建QName  
            String namespaceURI = "http://ws.cxf.hqh.com";  
            String localPart = "license";  
            String prefix = "ns";  
            QName qname = new QName(namespaceURI, localPart, prefix);  
  
            //创建需要使用header进行传输的对象  
            String sendObj = "license for webservice";  
              
            //创建DataBinding  
            DataBinding dataBinding = new JAXBDataBinding(String.class);  
              
            //创建Header  
            Header header = new Header(qname, sendObj, dataBinding);  
            //将header加入到SOAP头集合中  
            headers.add(header);  
        } catch (JAXBException e) {  
            e.printStackTrace();  
            throw new Fault(e);  
        }  
		
		
	}
	


	
	
	

}
