package com.jan.cxf.client;

import java.io.StringReader;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.jan.cxf.demo.WebServiceDemo;

public class SpringWsClient {

	private ApplicationContext context;

	@Before
	public void before() {
		context = new ClassPathXmlApplicationContext("classpath:spring-cxf-client.xml");
	}

	@Test
	public void proxyClient() {
		WebServiceDemo port = context.getBean("simpleService", WebServiceDemo.class);
		String ret = port.helloWs("proxy");
		System.out.println(ret);
	}

//	@Test
	public void templateClient() {
//		String message = "<queryPeopleByID  xmlns=\"http://test.cxfws.com\">1231ss</queryPeopleByID> "; 
		
//		WebServiceTemplate simpleService = (WebServiceTemplate) context.getBean("webServiceTemplate");
//		StreamSource source = new StreamSource(new StringReader(message));
//		StreamResult result = new StreamResult(System.out);
//		simpleService.sendSourceAndReceiveToResult(source, result);
	}

}
