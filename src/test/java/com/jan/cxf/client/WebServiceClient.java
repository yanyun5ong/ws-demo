package com.jan.cxf.client;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import javax.xml.ws.handler.MessageContext;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;

import com.jan.cxf.demo.WebServiceDemo;
import com.jan.cxf.interceptor.UnionOutInterceptor;

public class WebServiceClient {

    public static void main(String[] args) throws Exception {
        WebServiceClient client = new WebServiceClient();
        client.invokeWsPlanC();
    }

    /**
     * 通过反向代理生成客户端
     */
    private void invokeWsPlanA() {
        JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        factory.setServiceClass(WebServiceDemo.class);
        factory.setAddress("http://localhost:8080/ws-demo/ws/webServiceDemo?wsdl");

        factory.getOutInterceptors().add(new UnionOutInterceptor());
         factory.getOutInterceptors().add(new LoggingOutInterceptor());
        // factory.getInInterceptors().add(new LoggingInInterceptor());//暂时屏蔽掉返回值的日志
        WebServiceDemo service = (WebServiceDemo) factory.create();
        String hello = service.helloWs("yanyunsong");
        System.out.println(hello);
    }

    /**
     * 动态生成
     */
    private void invokeWsPlanB() throws Exception {
        JaxWsDynamicClientFactory factory = JaxWsDynamicClientFactory.newInstance();
        Client client = factory.createClient("http://localhost:8080/ws-demo/ws/webServiceDemo?wsdl");
        Object[] result = client.invoke("helloWs", "invokeWsPlanB");

        for (int i = 0; i < result.length; i++) {
            System.out.println(result[i]);
        }
    }

    private void invokeWsPlanC() throws MalformedURLException  {
        String wsUrl = "http://localhost:8080/ws-demo/ws/webServiceDemo?wsdl";
        URL url = new URL(wsUrl);
        QName qname = new QName("http://demo.cxf.jan.com/", "webServiceDemo");

        Service service = Service.create(url, qname);
        WebServiceDemo port = service.getPort(WebServiceDemo.class);

        /******************* UserName & Password ******************************/
        Map<String, Object> req_ctx = ((BindingProvider)port).getRequestContext();
        req_ctx.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, wsUrl);

        Map<String, List<String>> headers = new HashMap<String, List<String>>();
        headers.put("Username", Collections.singletonList("boc"));
        headers.put("Password", Collections.singletonList("boc111"));
        req_ctx.put(MessageContext.HTTP_REQUEST_HEADERS, headers);
        /**********************************************************************/

        String rt = port.helloWs("world");
        System.out.println(rt);
    }

}
