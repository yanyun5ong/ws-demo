package com.jan.cxf.demo.impl;

import javax.jws.WebService;

import com.jan.cxf.demo.WebServiceDemo;

@WebService(endpointInterface = "com.jan.cxf.demo.WebServiceDemo", serviceName = "webServiceDemo", targetNamespace="http://demo.cxf.jan.com/")
public class WebServiceDemoImpl implements WebServiceDemo {

    @Override
    public String helloWs(String name) {
        System.out.println("server say: recieve success");
//        System.out.println("server sleep 2 seconds...");
//        try {
//            Thread.sleep(2*1000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        System.out.println("server wake up...");
        return "hello " + name;
    }

}
