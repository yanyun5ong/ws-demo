package com.jan.cxf.demo;

import javax.jws.WebParam;
import javax.jws.WebService;




@WebService
public interface WebServiceDemo {

    public String helloWs(@WebParam(name = "userName") String name);
}
